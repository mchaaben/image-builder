FROM docker:latest

ENV KUBECTL_VERSION "1.19.2"

RUN apk --no-cache upgrade
RUN apk add --update bash ca-certificates git python3 jq

RUN apk add --update py-pip curl make openssl groff

RUN curl -L https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl &&\
    chmod +x /usr/local/bin/kubectl

RUN curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.17.9/2020-08-04/bin/linux/amd64/aws-iam-authenticator &&\ 
    curl -o aws-iam-authenticator.sha256 https://amazon-eks.s3.us-west-2.amazonaws.com/1.17.9/2020-08-04/bin/linux/amd64/aws-iam-authenticator.sha256 &&\
    openssl sha1 -sha256 aws-iam-authenticator &&\
    chmod +x ./aws-iam-authenticator &&\
    mv aws-iam-authenticator /usr/local/bin/aws-iam-authenticator

RUN pip install awscli

RUN pip install yamllint yq

# cleanup
RUN rm /var/cache/apk/*
RUN rm -rf /tmp/*

CMD bash